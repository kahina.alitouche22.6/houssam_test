import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.mycompany.junit_test.DetermineGuess;

public class DetermineGuessTest {
	DetermineGuess guesstest = new DetermineGuess(); 
	@Test
    public void testinvalid() {
    	assertEquals("Your guess is invalid", guesstest.determineGuesss(-1, 22, 1));
    }
	@Test
    public void testinvalid2() {
    	assertEquals("Your guess is invalid", guesstest.determineGuesss(200, 22, 1));
    }
	
    
    @Test
    public void testCorrect() {
        assertEquals("Correct!\nTotal Guesses: 1", guesstest.determineGuesss(22, 22, 1));
    }
    
    @Test
    public void testHigh() {
        assertEquals("Your guess is too high, try again.\nTry Number: 1", guesstest.determineGuesss(70, 50, 1));
    }
    
    @Test
    public void testlow() {
    assertEquals("Your guess is too low, try again.\nTry Number: 1", guesstest.determineGuesss(1,50,1));
    }
}
