package com.mycompany.junit_test;

public class DetermineGuess {
	public String determineGuesss(int userAnswer, int computerNumber, int count){
		 if (userAnswer <=0 || userAnswer >100) {
		 return "Your guess is invalid";
		 }
		 else if (userAnswer == computerNumber ){
		 return "Correct!\nTotal Guesses: " + count;
		 }
		 else if (userAnswer > computerNumber) {
		 return "Your guess is too high, try again.\nTry Number: " + count;
		 }
		 else if (userAnswer < computerNumber) {
		 return "Your guess is too low, try again.\nTry Number: " + count;
		 }
		 else {
		 return "Your guess is incorrect\nTry Number: " + count;
		 }
		 }
}
