/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.junit_test;

/**
 *
 * @author kahina
 */
public class Junit_Test {

    public static void main(String[] args) {
        Calculatrice calc = new Calculatrice();
        
        System.out.println(calc.addition(1, 3));
        System.out.println(calc.addition(-1, 3));
        System.out.println(calc.addition(0, 3));
        System.out.println(calc.soustraction(1, 3));
        System.out.println(calc.soustraction(3, 1));
        System.out.println(calc.soustraction(1, -3));
        System.out.println(calc.multiplication(1, 3));
        System.out.println(calc.multiplication(-1, 3));
        System.out.println(calc.division(2, 3));
        System.out.println(calc.division(-2, 3));
        System.out.println(calc.division(2, 1));
        
        
        System.out.println("Factorielle: " + calc.factoIte(4));
        System.out.println("Factorielle: " + calc.factoRec(4));
        System.out.println("Factorielle: " + calc.factoIte(1));
        System.out.println("Factorielle: " + calc.factoIte(0));







    }
}
