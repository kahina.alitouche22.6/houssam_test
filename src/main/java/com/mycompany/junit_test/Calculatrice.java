/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.junit_test;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kahina
 */
public class Calculatrice {
    public int addition(int a, int b){
        return a + b;
    }
    
    public int soustraction(int a, int b) {
        return a - b;
    }
    
    public int multiplication(int a, int b) {
        return a * b;
    }
    
    public int division(int a, int b) {
        if (b == 0 ) {
            System.err.println("Impossible de diviser par 0");
            return Integer.MIN_VALUE;
        }
        return a / b;
    }
    
    public int factoIte(int nb) {
        int facto = 1;
        for(int i = 1; i <= nb; i++) {
            facto *= i;
        }
        return facto;
    }
    
    public int factoRec(int nb, int sommeFacto) {
        if (nb <= 1) {
            return sommeFacto;            
        }
        
        return nb * factoRec(nb - 1, sommeFacto);
    }
    
    public int factoRec(int nb) {
        return factoRec(nb, 1);
    }
    
    public int calcul(int a, int b, String operateur){
        int res = 0;
        switch(operateur) {
            case "+":
                res = addition(a, b);
                break;
            
            case "-":
                res = soustraction(a, b);
                break;
                
            case "*":
                res = multiplication(a, b);
                break;
                
            case "/":
                res = division(a, b);
                break;
                
            default:
                throw new IllegalArgumentException("Opérateur non traité!");           
        }
        return res;
    }
    
    
    synchronized public boolean waitTest(int i) {
        try {
            wait(i);
        } catch (InterruptedException ex) {
            Logger.getLogger(Calculatrice.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}
